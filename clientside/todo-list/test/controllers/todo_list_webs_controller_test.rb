require 'test_helper'

class TodoListWebsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @todo_list_web = todo_list_webs(:one)
  end

  test "should get index" do
    get todo_list_webs_url
    assert_response :success
  end

  test "should get new" do
    get new_todo_list_web_url
    assert_response :success
  end

  test "should create todo_list_web" do
    assert_difference('TodoListWeb.count') do
      post todo_list_webs_url, params: { todo_list_web: { description: @todo_list_web.description, title: @todo_list_web.title } }
    end

    assert_redirected_to todo_list_web_url(TodoListWeb.last)
  end

  test "should show todo_list_web" do
    get todo_list_web_url(@todo_list_web)
    assert_response :success
  end

  test "should get edit" do
    get edit_todo_list_web_url(@todo_list_web)
    assert_response :success
  end

  test "should update todo_list_web" do
    patch todo_list_web_url(@todo_list_web), params: { todo_list_web: { description: @todo_list_web.description, title: @todo_list_web.title } }
    assert_redirected_to todo_list_web_url(@todo_list_web)
  end

  test "should destroy todo_list_web" do
    assert_difference('TodoListWeb.count', -1) do
      delete todo_list_web_url(@todo_list_web)
    end

    assert_redirected_to todo_list_webs_url
  end
end
