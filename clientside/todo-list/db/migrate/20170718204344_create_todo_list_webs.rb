class CreateTodoListWebs < ActiveRecord::Migration[5.0]
  def change
    create_table :todo_list_webs do |t|
      t.string :title
      t.text :description
      
      t.timestamps
    end
  end
end
