class AddDuedateToTodolist < ActiveRecord::Migration[5.0]
  def change
    add_column :todo_list_webs, :due_date, :date
  end
end
