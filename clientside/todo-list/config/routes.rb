Rails.application.routes.draw do
  resources :todo_list_webs
  resources :microposts
  resources :users
   root 'todo_list_webs#index'
end