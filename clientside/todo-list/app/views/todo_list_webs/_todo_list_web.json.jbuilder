json.extract! todo_list_web, :id, :title, :description, :created_at, :updated_at, :date
json.url todo_list_web_url(todo_list_web, format: :json)
