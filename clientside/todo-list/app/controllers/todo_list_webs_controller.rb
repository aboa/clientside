class TodoListWebsController < ApplicationController
  before_action :set_todo_list_web, only: [:show, :edit, :update, :destroy]

  # GET /todo_list_webs
  # GET /todo_list_webs.json
  def index
    @todo_list_webs = TodoListWeb.all
  end

  # GET /todo_list_webs/1
  # GET /todo_list_webs/1.json
  def show
  end

  # GET /todo_list_webs/new
  def new
    @todo_list_web = TodoListWeb.new
  end

  # GET /todo_list_webs/1/edit
  def edit
  end

  # POST /todo_list_webs
  # POST /todo_list_webs.json
  def create
    @todo_list_web = TodoListWeb.new(todo_list_web_params)

    respond_to do |format|
      if @todo_list_web.save
        format.html { redirect_to @todo_list_web, notice: 'Todo list web was successfully created.' }
        format.json { render :show, status: :created, location: @todo_list_web }
      else
        format.html { render :new }
        format.json { render json: @todo_list_web.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /todo_list_webs/1
  # PATCH/PUT /todo_list_webs/1.json
  def update
    respond_to do |format|
      if @todo_list_web.update(todo_list_web_params)
        format.html { redirect_to @todo_list_web, notice: 'Todo list web was successfully updated.' }
        format.json { render :show, status: :ok, location: @todo_list_web }
      else
        format.html { render :edit }
        format.json { render json: @todo_list_web.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /todo_list_webs/1
  # DELETE /todo_list_webs/1.json
  def destroy
    @todo_list_web.destroy
    respond_to do |format|
      format.html { redirect_to todo_list_webs_url, notice: 'Todo list web was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todo_list_web
      @todo_list_web = TodoListWeb.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def todo_list_web_params
      params.require(:todo_list_web).permit(:title, :description, :date)
    end
end
